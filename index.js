const express = require('express')
const cookieParser = require('cookie-parser')
var cors = require('cors')
const crypto = require('crypto');
const mongoose = require('mongoose');
const { stringify } = require('querystring');
var bodyParser = require('body-parser');
var db = "mongodb+srv://vissu:vissu@cluster0.qqd2s.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
mongoose
    .connect(db, { 
        useNewUrlParser: true,
      })
    .then(() => console.log('MongoDB connected...'))
    .catch(err => console.log(err));
const Schema = mongoose.Schema
const sessionsData = new Schema({
    name: String,
    sessionId: String,
})
const messageSchema = new Schema({
    sessionId: String,
    message: String
});
const messageModel = mongoose.model("messageInfo",messageSchema)
const sessionInfoModedl = mongoose.model("sessionInfo",sessionsData)
const app = express()
app.use(cors({
    origin : "http://localhost:3000",
    credentials: true, // <= Accept credentials (cookies) sent by the client
}));
app.use(bodyParser.json());
// in latest body-parser use like below.
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json())
app.use(cookieParser())
var generateSession = (name) => {
    var sessionId = crypto.randomBytes(16).toString('base64')
    var sessionIdvar = new sessionInfoModedl({
        name:name,
        sessionId: sessionId
      });
    const flag=true;
    sessionIdvar.save((err, doc) => {
    if (err){
        flag = false;
    }});
    if (flag)
    return sessionId;
    else 
    return false;
}

function checkSessionId(clientSessionId){
    var flag=true;
    var check = sessionInfoModedl.findOne({
        sessionId: clientSessionId
   }, function (err, existingUser) {
       if (err){
           console.log(err)
           flag=false;
       }
       else{
           console.log(`${clientSessionId} Found`)
           return true;
       }
   });

   if (flag)
   return true;
   
   return false;
    
}

function validateCookie(req,res,next) {
    var clientCookie = req.cookies
    if ('session_id' in clientCookie){
        var clientSessionId = clientCookie.session_id
        if (checkSessionId(clientSessionId)){
            next()
        }
        else{
            res.status(400).json({msg:"SessionId not valid"});
        }
    }else{
        res.status(400).json({msg:"SessionId not valid"});
    }

}

app.get('/validate',validateCookie,(req,res) =>{
    res.set('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.status(200).json({msg:"Cookie generated"})
})

app.get('/secret/:sessionId',(req,res)=>{
    console.log("Heyyy rammmm")
    var sessionId = req.params.sessionId
    sessionId = String(sessionId)
    var k = sessionInfoModedl.findOne({ sessionId: sessionId }, function (err, obj) {
        if (err){
            res.status(400).json({msg:"URL invalid"})
            console.log(err)
        }
        else{
            res.status(200).json({result: obj})
            console.log("Result : ", obj);
        }
        return obj;
    });
    console.log(req.param)
})

app.post('/signin',(req,res) => {
    console.log(req)
    var names = req.body.names
    console.log(req.body)
    var sessionId = generateSession(names)
    if (sessionId){
    res.cookie('session_id',sessionId)
    res.set('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.status(200).json({sessionId: `${sessionId}`})
    res.end()
    }
    else
    {
        res.status(400).json({msg:"Session generation error in DB"})
    }
})

app.post('/message',(req,res)=>{
    var sessionId= req.body.sessionId
    var message = req.body.message
    var messageEntry = new messageModel({
        sessionId: sessionId,
        message: message
    });
    const flag=true;
    messageEntry.save((err, obj) => {
    if (err){
        res.status(400).json({msg : "error in sending message"})
    }});
    res.status(200).json({msg:"Message sent"})
})

app.get('/messages/:sessionId',(req,res)=>{
    var sessionId = req.params.sessionId
    messageModel.find({sessionId:sessionId}, function(err, obj) {
        if (!err) { 
            console.log(obj);
            res.status(200).json(obj);
        }
        else {
            res.status(400).json({msg:"Unable to fetch Messages"})
        }
    });
});

app.listen(5000, () =>{
    console.log("App is running")
})